set -e

# prepare test environment
prepare() {
    cp -a default.build.properties Gemfile Rakefile rakelib spec test "$AUTOPKGTEST_TMP"
    cp -a /usr/share/jruby/bin "$AUTOPKGTEST_TMP/bin"
    ln -s /usr/share/jruby/lib "$AUTOPKGTEST_TMP/lib"
    export LANG=C.UTF-8
    export LC_ALL=C.UTF-8
    export CI=1
    export JOBS=2

    # create an additional directory for GEM_PATH
    # with extra gems needed to run the testsuite
    export GEM_TEMP="$AUTOPKGTEST_TMP/gem_temp"
    mkdir -p "${GEM_TEMP}/gems" "${GEM_TEMP}/specifications"

    # add test-required gems from system
    RUBY_API_VERSION=$(/usr/bin/ruby -e "require 'ruby_debian_dev'; puts RubyDebianDev::RUBY_API_VERSION.values.first")
    echo "installing system gems into jruby:"
	for gem in diff-lcs matrix minitest net-ftp power_assert prime rake rexml rspec rspec-core rspec-expectations rspec-mocks rspec-support test-unit; do
		# some gems are copied from libruby*, others from local rubygems
        gem_version=$(find "/usr/lib/ruby/gems/${RUBY_API_VERSION}/gems" /usr/share/rubygems-integration/all/gems \
            -maxdepth 1 -type d -regex ".*/${gem}-[0-9\.]+" -printf "%f\n" -quit)
		if test -e "/usr/lib/ruby/gems/${RUBY_API_VERSION}/gems/${gem_version}"; then
			gembase="/usr/lib/ruby/gems/${RUBY_API_VERSION}"
		else
			gembase=/usr/share/rubygems-integration/all
		fi
		echo "* ${gem_version}"
		ln -s "${gembase}/gems/${gem_version}" "${GEM_TEMP}/gems"
		ln -s "${gembase}/specifications/${gem_version}.gemspec" "${GEM_TEMP}/specifications"
	done

    # copy/fixup the rake binary
    cp /usr/bin/rake ./bin
    sed -i '1s|^#!.*|#!/usr/bin/jruby|' ./bin/rake

    export GEM_PATH="/usr/share/jruby/lib/ruby/gems/shared:${GEM_TEMP}"
}

spec_skip() {
    test_name="$1"
    test_file="$2"
    if [ "$test_name" = "ALL" ]; then
      sed -i "/it.*do$/a \    skip 'skipped for autopkgtest'" "${test_file}"
    else
      sed -i "/it \"${test_name}\" do$/a \    skip 'skipped for autopkgtest'" "${test_file}"
    fi
    echo "excluded test ${exclude_file}::${test_name}"
}

test_skip() {
    test_name="$1"
    test_file="$2"
    sed -i "/def ${test_name}$/a \    omit 'skipped for autopkgtest'" "${test_file}"
    echo "excluded test: ${exclude_file}::${test_name}"
}

mri_exclude() {
    test_name="$1"
    exclude_file="test/mri/excludes/$2"
    if [ ! -d "$(dirname ${exclude_file})" ]; then
        mkdir "$(dirname ${exclude_file})"
    fi
    if [ -f "${exclude_file}" ]; then
        if grep -q "^exclude :${test_name}," "${exclude_file}"; then
            echo "Warning: ${test_name} already exists in ${exclude_file}"
            return
        fi
        sed -i -e '$a\' "${exclude_file}"
    fi
    echo "exclude :${test_name}, \"autopkgtest\"" >> "${exclude_file}"
    echo "excluded test: ${exclude_file}::${test_name}"
}

# run 1 test at a time and output test names
# so we can more easily catch hanging tests
debug_tests() {
    export JOBS=1
    # modifies ADDITIONAL_TEST_OPTIONS
    sed -i 's/--tty=no/-v/' "$AUTOPKGTEST_TMP/rakelib/test.rake"
    # modifies TESTOPT
    sed -i 's/--no-use-color/--no-use-color -v/' "$AUTOPKGTEST_TMP/rakelib/test.rake"
    echo "** debugging mode enabled **"
}
