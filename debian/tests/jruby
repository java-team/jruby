#!/bin/sh

. "$(dirname $0)/common.sh"

prepare

cd "$AUTOPKGTEST_TMP"

# extra java libs required
mkdir test/target
ln -s /usr/share/java/junit4.jar test/target/junit.jar
ln -s /usr/share/java/jakarta-annotation-api.jar test/target/annotation-api.jar
ln -s /usr/share/java/jarjar.jar test/target/jarjar.jar

# compile a few test dependencies
gcc test/jruby/testapp/testapp.c -o test/jruby/testapp/testapp
javac -cp lib/jruby.jar:test/target/junit.jar:test/target/annotation-api.jar -d test/target/test-classes \
    test/org/jruby/test/*.java \
    test/org/jruby/javasupport/test/*.java \
    test/org/jruby/javasupport/test/*/*.java \
    test/*.java

# some test cases look for these specific files
cp "$OLDPWD/pom.xml" .
cp bin/jruby bin/jruby.sh

# when $TMPDIR is /tmp and $PWD is under there
# it causes the test_jrubyc.rb/test_target test case to fail
TMPDIR="$(mktemp -d)"
export TMPDIR

# workaround for getsockopt failures
# see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1058703
# and https://github.com/jruby/jruby/issues/8339
test_skip test_tcp_info test/jruby/test_socket.rb
for t in cnt idle intvl; do
  test_skip "test_tcp_socket_get_keep_${t}" test/jruby/test_socket.rb
  sed -i "/def test_tcp_socket_get_keep_${t}/,/end/{/ensure/d;/socket\.close/d}" test/jruby/test_socket.rb
done

# skip failing tests on specific architectures
if [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armel" ] || \
     [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "armhf" ]; then
  test_skip test_file_open_utime test/jruby/test_file.rb
  test_skip test_file_utime_nil test/jruby/test_file.rb
  test_skip test_file_mtime_after_fileutils_touch test/jruby/test_file.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "ppc64el" ]; then
  test_skip test_premature_close_raises_appropriate_errors test/jruby/test_io.rb
elif [ "$(dpkg-architecture -qDEB_BUILD_ARCH)" = "s390x" ]; then
  test_skip test_file_open_utime test/jruby/test_file.rb
  test_skip test_file_utime_nil test/jruby/test_file.rb
fi

# run a single test
#jruby -I. test/jruby/test_loading_builtin_libraries.rb

# run the full rspec testsuite
jruby -S rake test:jruby
